#!/bin/sh
set -o errexit
set -o nounset

export RUSTFLAGS="-Dwarnings"

echo "========== FORMAT CODE =========="
status_before=$(git status --porcelain)
cargo fmt
if [ "$status_before" != "$(git status --porcelain)" ]; then
    echo "There were changes in the formatting. Aborting build."
    exit 1
fi
echo "============ CLIPPY ============="
cargo clippy
echo "============ BUILD =============="
cargo build
echo "=========== CBINDGEN ============"
mkdir -p include/rulsof
cbindgen --config cbindgen.toml --crate rulsof-c --output include/rulsof/rulsof.h
echo "========= C++ EXAMPLE ==========="
echo "============ BUILD =============="
cd example
rm -rf build && mkdir -p build && cd build
export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
cmake -DCMAKE_BUILD_TYPE=Debug .. && make
echo "============= TEST =============="
./lsof_cpp_test
