#include "rulsof/rulsof_cpp.h"

#define CATCH_CONFIG_COLOUR_NONE
#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

#include "unistd.h"

#include <cstdlib>
#include <sstream>
#include <optional>

std::string process_path;

std::string get_tmp_dir() {
    const char* temp_locations[] = { "TEMP", "TMP", "TMPDIR" };
    for (auto temp_location : temp_locations) {
        if (auto tmpdir = getenv(temp_location); tmpdir != NULL) {
            return tmpdir;
        }
    }
    return "/tmp";
}

struct TemporaryFile {
    int fd;
    std::string name;
};

std::optional<TemporaryFile> create_temporary_file() {
    std::ostringstream temp_file_pattern_stream;
    temp_file_pattern_stream << get_tmp_dir() << "/lsof_test_XXXXXX";

    TemporaryFile file_record;
    file_record.name = temp_file_pattern_stream.str();
    file_record.fd = mkstemp(file_record.name.data());
    if (file_record.fd > -1) {
        return file_record;
    } else {
        return std::nullopt;
    }
}

TEST_CASE("rulsof C++ interface", "[test]") {
    SECTION("lsof call returns some values") {
        rulsof_cpp::Lsof lsof;
        auto output = lsof.lsof("-p", std::to_string(getpid()));
        REQUIRE(!output.empty());
    }

    SECTION("lsof call returns expected records") {
        auto temp_file_optional = create_temporary_file();
        REQUIRE(temp_file_optional != std::nullopt);
        auto temp_file = *temp_file_optional;

        rulsof_cpp::Lsof lsof;
        auto lsof_output = lsof.lsof(temp_file.name);
        REQUIRE(lsof_output.size() == 1);
        for (auto& [key, value] : lsof_output) {
            REQUIRE(std::to_string(getpid()) == key);
            auto command_path = value.process_fields.at("c");
            REQUIRE(process_path.find(command_path) != std::string::npos);
        }

        close(temp_file.fd);
        unlink(temp_file.name.c_str());
    }

    SECTION("fails if lsof cannot be found") {
        try {
            auto lsof_options = rulsof_cpp::Lsof::Options{ "./non/existing/binary" };
            rulsof_cpp::Lsof lsof(lsof_options);
            lsof.lsof("-p", std::to_string(getpid()));
            FAIL();
        } catch(const std::runtime_error& e) {
            std::string error_message = e.what();
            REQUIRE(error_message.find("'e'") != std::string::npos);
            REQUIRE(error_message.find("ExecutionFailed") != std::string::npos);
        }
    }
}

int main(int argc, char* argv[])
{
    Catch::Session session;

    process_path = std::string(argv[0]);

    return session.run(argc, argv) == 0 ? 0 : 1;
}
