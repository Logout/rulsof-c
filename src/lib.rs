extern crate rulsof;

use std::os::unix::ffi::OsStrExt;

use rulsof::Lsof;

// Input
#[repr(C)]
pub struct LsofArguments {
    argument: *const u8,
    length: usize,
    next_arg: *const LsofArguments,
}
#[repr(C)]
pub struct LsofOptions {
    executable_path: *const u8,
    executable_path_length: usize,
}
// Output
#[repr(C)]
pub struct Field {
    key: *const u8,
    key_length: usize,
    value: *const u8,
    value_length: usize,
    next_entry: *mut Field,
}
#[repr(C)]
pub struct Files {
    file_fields: *mut Field,
    next_entry: *mut Files,
}
#[repr(C)]
pub struct ProcessEntry {
    process_fields: *mut Field,
    files: *mut Files,
}
#[repr(C)]
pub struct LsofOutput {
    pid: *const u8,
    pid_length: usize,
    value: ProcessEntry,
    next_entry: *mut LsofOutput,
}
#[repr(u8)]
pub enum LsofErrorCode {
    ExecutionFailed = b'e',
    LsofFailed = b'l',
    LsofNoOutput = b'n',
    LsofMalformedOutput = b'm',
    ParserFailed = b'p',
}
#[repr(C)]
pub struct LsofError {
    error_code: u8,
    error_msg: *const u8,
    error_msg_length: usize,
}
#[repr(C)]
pub struct LsofResult {
    resource_ptr: *mut std::os::raw::c_void,
    lsof_output: *mut LsofOutput,
    lsof_error: *mut LsofError,
}

#[no_mangle]
pub extern "C" fn lsof(args: *const LsofArguments, options: *const LsofOptions) -> *mut LsofResult {
    let mut lsof_args = vec![];
    let mut lsof_option: Option<Lsof> = None;
    let mut current_arg = args;
    unsafe {
        loop {
            if let Some(arg_as_reference) = current_arg.as_ref() {
                let payload_as_slice =
                    std::slice::from_raw_parts(arg_as_reference.argument, arg_as_reference.length);
                let payload = std::ffi::OsStr::from_bytes(payload_as_slice);
                lsof_args.push(payload);
                if arg_as_reference.next_arg.is_null() {
                    break;
                } else {
                    current_arg = arg_as_reference.next_arg;
                }
            }
        }
        let options_ptr = options;
        if let Some(options_as_reference) = options_ptr.as_ref() {
            let path_as_slice = std::slice::from_raw_parts(
                options_as_reference.executable_path,
                options_as_reference.executable_path_length,
            );
            let executable_path = std::ffi::OsStr::from_bytes(path_as_slice);
            lsof_option = Some(Lsof { executable_path });
        }
    }
    let mut lsof_result = LsofResult {
        resource_ptr: std::ptr::null_mut(),
        lsof_output: std::ptr::null_mut(),
        lsof_error: std::ptr::null_mut(),
    };
    let lsof = lsof_option.unwrap_or_default();
    let rulsof_result = lsof.lsof(lsof_args);
    match rulsof_result {
        Ok(rulsof_output) => {
            let result_resource = Box::new(rulsof_output);
            let mut record_vector = vec![];
            result_resource
                .iter()
                .for_each(|(pid, record)| record_vector.push((pid.as_str(), record)));
            lsof_result.lsof_output = create_lsof_output_linked_list(&record_vector);
            lsof_result.resource_ptr = Box::into_raw(result_resource).cast()
        }
        Err(error) => {
            let error_message = Box::new(format!("{}", error));
            let lsof_error = Box::new(LsofError {
                error_code: map_error_to_code(error),
                error_msg: error_message.as_ptr(),
                error_msg_length: error_message.len(),
            });
            lsof_result.lsof_error = Box::into_raw(lsof_error);
            lsof_result.resource_ptr = Box::into_raw(error_message).cast();
        }
    }
    Box::into_raw(Box::new(lsof_result))
}

fn create_lsof_output_linked_list(
    elements: &[(&str, &rulsof::parser::ProcessEntry)],
) -> *mut LsofOutput {
    if let Some((first, rest)) = elements.split_first() {
        let (pid, record) = first;
        let mut process_fields_vector = vec![];
        record
            .process_fields
            .iter()
            .for_each(|(key, value)| process_fields_vector.push((key.as_str(), value.as_str())));
        let process_fields = create_linked_list(&process_fields_vector);
        let files = create_file_linked_list(&record.files);
        let process_entry = ProcessEntry {
            process_fields,
            files,
        };
        let mut lsof_output = Box::new(LsofOutput {
            pid: pid.as_ptr(),
            pid_length: pid.len(),
            value: process_entry,
            next_entry: std::ptr::null_mut(),
        });
        if !rest.is_empty() {
            let next_entry = create_lsof_output_linked_list(rest);
            lsof_output.next_entry = next_entry;
        }
        Box::into_raw(lsof_output)
    } else {
        std::ptr::null_mut()
    }
}

fn create_linked_list(elements: &[(&str, &str)]) -> *mut Field {
    if let Some((first, rest)) = elements.split_first() {
        let (key, value) = first;
        let mut new_element = Box::new(Field {
            key: key.as_ptr(),
            key_length: key.len(),
            value: value.as_ptr(),
            value_length: value.len(),
            next_entry: std::ptr::null_mut(),
        });
        if !rest.is_empty() {
            let next_entry = create_linked_list(rest);
            new_element.next_entry = next_entry;
        }
        Box::into_raw(new_element)
    } else {
        std::ptr::null_mut()
    }
}

fn create_file_linked_list(elements: &[std::collections::HashMap<String, String>]) -> *mut Files {
    if let Some((first, rest)) = elements.split_first() {
        let mut file_fields_vector = vec![];
        first
            .iter()
            .for_each(|(key, value)| file_fields_vector.push((key.as_str(), value.as_str())));
        let mut files = Box::new(Files {
            file_fields: create_linked_list(&file_fields_vector),
            next_entry: std::ptr::null_mut(),
        });
        if !rest.is_empty() {
            let next_entry = create_file_linked_list(rest);
            files.next_entry = next_entry;
        }
        Box::into_raw(files)
    } else {
        std::ptr::null_mut()
    }
}

fn map_error_to_code(error: rulsof::lsof_error::LsofError) -> u8 {
    (match error {
        rulsof::lsof_error::LsofError::ExecutionFailed(_) => LsofErrorCode::ExecutionFailed,
        rulsof::lsof_error::LsofError::LsofFailed => LsofErrorCode::LsofFailed,
        rulsof::lsof_error::LsofError::LsofNoOutput => LsofErrorCode::LsofNoOutput,
        rulsof::lsof_error::LsofError::LsofMalformedOutput(_) => LsofErrorCode::LsofMalformedOutput,
        rulsof::lsof_error::LsofError::ParserFailed(_) => LsofErrorCode::ParserFailed,
    }) as u8
}

/// # Safety
/// This function is safe if the pointers returned by lsof() are untouched,
/// as the data structure is simple and the memory layout is fixed.
#[no_mangle]
pub unsafe extern "C" fn lsof_free(resource_pointer: *mut LsofResult) {
    if resource_pointer.is_null() {
        return;
    }
    let resource_box = Box::from_raw(resource_pointer);
    let rulsof_error = resource_box.lsof_error;
    if rulsof_error.is_null() {
        let mut current_output_pointer = resource_box.lsof_output;
        while !current_output_pointer.is_null() {
            let current_output_box = Box::from_raw(current_output_pointer);
            let mut current_process_fields_pointer = current_output_box.value.process_fields;
            while !current_process_fields_pointer.is_null() {
                let current_process_fields_box = Box::from_raw(current_process_fields_pointer);
                current_process_fields_pointer = current_process_fields_box.next_entry;
            }
            let mut current_files_pointer = current_output_box.value.files;
            while !current_files_pointer.is_null() {
                let current_files_box = Box::from_raw(current_files_pointer);
                let mut current_file_pointer = current_files_box.file_fields;
                while !current_file_pointer.is_null() {
                    let current_file_box = Box::from_raw(current_file_pointer);
                    current_file_pointer = current_file_box.next_entry;
                }
                current_files_pointer = current_files_box.next_entry;
            }
            current_output_pointer = current_output_box.next_entry;
        }
        let rulsof_resources: *mut rulsof::parser::LsofOutput = resource_box.resource_ptr.cast();
        Box::from_raw(rulsof_resources);
    } else {
        let rulsof_resources: *mut String = resource_box.resource_ptr.cast();
        Box::from_raw(rulsof_resources);
        Box::from_raw(rulsof_error);
    }
}
