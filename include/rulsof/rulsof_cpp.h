#pragma once

extern "C" {
#include "rulsof/rulsof.h"
}

#include <memory>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

namespace rulsof_cpp {
    using KeyValueMap = std::unordered_map<std::string, std::string>;

    struct ProcessEntry {
        KeyValueMap process_fields;
        std::vector<KeyValueMap> files;
    };

    using LsofOutput = std::unordered_map<std::string, ProcessEntry>;

    inline void convert_to_lsof_arguments(
            const std::vector<std::string>& arguments,
            std::vector<LsofArguments>& lsof_arguments
            ) {
        for (const auto& argument: arguments) {
            lsof_arguments.push_back({
                    reinterpret_cast<const unsigned char*>(argument.c_str()),
                    argument.size(),
                    nullptr
                    });
        }
        // link list
        if (lsof_arguments.size() > 1) {
            auto firstIter = lsof_arguments.begin();
            for (;;) {
                auto secondIter = firstIter + 1;
                if (secondIter == lsof_arguments.end()) {
                    break;
                }
                firstIter->next_arg = &(*secondIter);
                firstIter = secondIter;
            }
        }
    }

    inline void insert_field(
            KeyValueMap& target_map,
            const Field* field
            ) {
        std::string key(
                reinterpret_cast<const char*>(field->key),
                field->key_length
                );
        std::string value(
                reinterpret_cast<const char*>(field->value),
                field->value_length
                );
        target_map.emplace(key, value);
    }

    inline LsofOutput convert_to_cpp_types(const ::LsofOutput *lsof_output) {
        LsofOutput cpp_lsof_output;
        while (lsof_output) {
            std::string pid(
                    reinterpret_cast<const char*>(lsof_output->pid),
                    lsof_output->pid_length
                    );
            ProcessEntry process_entry;
            auto process_fields = lsof_output->value.process_fields;
            while (process_fields) {
                insert_field(process_entry.process_fields, process_fields);
                process_fields = process_fields->next_entry;
            }
            auto files = lsof_output->value.files;
            while (files) {
                KeyValueMap file_field_map;
                auto file_fields = files->file_fields;
                while (file_fields) {
                    insert_field(file_field_map, file_fields);
                    file_fields = file_fields->next_entry;
                }
                process_entry.files.push_back(file_field_map);
                files = files->next_entry;
            }
            cpp_lsof_output.emplace(pid, process_entry);
            lsof_output = lsof_output->next_entry;
        }
        return cpp_lsof_output;
    }

    class Lsof {
        public:
            struct Options {
                std::string executable_path;
            };

            Lsof(std::optional<const Options> options = std::nullopt)
                : options(std::move(options))
            {}

            template<typename... T>
                LsofOutput lsof(T... args) {
                    std::vector<std::string> arguments{ args... };
                    std::vector<LsofArguments> lsof_arguments;
                    convert_to_lsof_arguments(arguments, lsof_arguments);

                    auto argument_ptr = lsof_arguments.empty()
                        ? nullptr
                        : lsof_arguments.data();
                    auto lsof_result = call_lsof(argument_ptr);

                    if (lsof_result == nullptr) {
                        throw std::runtime_error("No lsof result set");
                    }
                    if (lsof_result->lsof_output == nullptr) {
                        auto error_code = '-';
                        std::string error_message;
                        auto lsof_error = lsof_result->lsof_error;
                        if (lsof_error) {
                            error_code = lsof_error->error_code;
                            error_message = std::string(
                                    reinterpret_cast<const char*>(
                                        lsof_error->error_msg
                                        ),
                                    lsof_error->error_msg_length);
                        }
                        std::stringstream error_message_stream;
                        error_message_stream
                            << "Lsof error: Code '"
                            << error_code
                            << "', message: "
                            << error_message;
                        throw std::runtime_error(error_message_stream.str());
                    }

                    return convert_to_cpp_types(lsof_result->lsof_output);
                }

        private:
            using LsofFreeT = decltype(&lsof_free);

            const std::optional<Options> options;

            std::unique_ptr<LsofResult, LsofFreeT>
                call_lsof(const LsofArguments* args) {
                    std::unique_ptr<LsofOptions> options_ptr;
                    if (options != std::nullopt) {
                        if (!options->executable_path.empty()) {
                            LsofOptions effective_options{
                                reinterpret_cast<const uint8_t*>(options->executable_path.data()),
                                    options->executable_path.size(),
                            };
                            options_ptr = std::make_unique<LsofOptions>(effective_options);
                        }
                    }

                    return std::unique_ptr<LsofResult, LsofFreeT>(
                            ::lsof(args, options_ptr.get()),
                            &lsof_free
                            );
                }
    };
}
